﻿function showTab(elm) {
	if (elm === 1) {
		$("#tab1, #btnTab1").addClass("active");
		$("#tab2, #btnTab2").removeClass("active");
		$("#tab3, #btnTab3").removeClass("active");
	} else if (elm === 2) {
		$("#tab2, #btnTab2").addClass("active");
		$("#tab1, #btnTab1").removeClass("active");
		$("#tab3, #btnTab3").removeClass("active");
	} else {
		$("#tab3, #btnTab3").addClass("active");
		$("#tab2, #btnTab2").removeClass("active");
		$("#tab1, #btnTab1").removeClass("active");
	}
}