﻿//custom slick slider
$(".mslick").slick({
	arrows: false,
	dots: true,
	appendDots: $(".mslick__dots")
});

$(".review__slick").slick({
	arrows: false,
	dots: true,
	appendDots: $(".review__dots")
});

$(".news__slick").slick({
	arrows: false,
	dots: true,
	appendDots: $(".news__dots")
});

$(".introduce__slick").slick({
	arrows: false,
	dots: true
});

$(".foundTour__slick").slick({
	appendArrows: $(".foundTour__slick--arrows"),
	mobileFirst: true,
	prevArrow: '<button type="button" class="slick-prev position-absolute"><i class="fa fa-chevron-left position-relative" aria-hidden="true"></i></button>',
	nextArrow: '<button type="button" class="slick-next position-absolute"><i class="fa fa-chevron-right position-relative" aria-hidden="true"></i></button>',
	slidesToShow: 3,
	arrows: false,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 6,
			arrows: true
		}
	}, {
		breakpoint: 600,
		settings: {
			slidesToShow: 4,
			arrows: true
		}
	}]
});

$(".onpost__more").slick({
	mobileFirst: true,
	prevArrow: '<button type="button" class="slick-prev position-absolute"><i class="fa fa-chevron-left position-relative" aria-hidden="true"></i></button>',
	nextArrow: '<button type="button" class="slick-next position-absolute"><i class="fa fa-chevron-right position-relative" aria-hidden="true"></i></button>',
	slidesToShow: 1,
	arrows: false,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 3,
			arrows: true
		}
	}, {
		breakpoint: 600,
		settings: {
			slidesToShow: 2,
			arrows: true
		}
	}]
});

$(".onnews__more").slick({
	mobileFirst: true,
	prevArrow: '<button type="button" class="slick-prev position-absolute"><i class="fa fa-chevron-left position-relative" aria-hidden="true"></i></button>',
	nextArrow: '<button type="button" class="slick-next position-absolute"><i class="fa fa-chevron-right position-relative" aria-hidden="true"></i></button>',
	slidesToShow: 1,
	arrows: false,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 2,
			arrows: true
		}
	}, {
		breakpoint: 600,
		settings: {
			slidesToShow: 2,
			arrows: true
		}
	}]
});

$(".onnews__slider").slick({
	prevArrow: '<button type="button" class="slick-prev position-absolute"><i class="fa fa-chevron-left position-relative" aria-hidden="true"></i></button>',
	nextArrow: '<button type="button" class="slick-next position-absolute"><i class="fa fa-chevron-right position-relative" aria-hidden="true"></i></button>',
	slidesToShow: 1
});

$('.onpost__slide--big').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: false,
	fade: true,
	asNavFor: '.onpost__slide--small'
});
$('.onpost__slide--small').slick({
	slidesToShow: 5,
	slidesToScroll: 1,
	asNavFor: '.onpost__slide--big',
	dots: false,
	focusOnSelect: true,
	nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
	prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>'
});

//show menu tablet & mobile
let isToggle = false;
$("#btnShowMenu").click(function () {
	if (isToggle) {
		$("#menu").css({
			'transform': 'translateY(20px)',
			'opacity': '0',
			'visibility': 'hidden'
		});
		isToggle = !isToggle;
	} else {
		$("#menu").css({
			'transform': 'translateY(0)',
			'opacity': '1',
			'visibility': 'visible'
		});
		isToggle = !isToggle;
	}
});