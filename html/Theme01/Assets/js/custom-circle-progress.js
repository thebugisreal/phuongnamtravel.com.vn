﻿// custom circle progress
$('#circle1').circleProgress({
	value: 0.85,
	size: 150,
	fill: {
		gradient: ["#ed5745", "#124f80"]
	},
	startAngle: 4.7,
	thickness: 6,
}).on('circle-animation-progress', function (event, progress) {
	$(this).find('strong').html(Math.round(85 * progress) + '<i>%</i>');
});

$('#circle2').circleProgress({
	value: 0.90,
	size: 150,
	fill: {
		gradient: ["#ed5745", "#124f80"]
	},
	startAngle: 4.7,
	thickness: 6,
}).on('circle-animation-progress', function (event, progress) {
	$(this).find('strong').html(Math.round(90 * progress) + '<i>%</i>');
});

$('#circle3').circleProgress({
	value: 0.80,
	size: 150,
	fill: {
		gradient: ["#ed5745", "#124f80"]
	},
	startAngle: 4.7,
	thickness: 6,
}).on('circle-animation-progress', function (event, progress) {
	$(this).find('strong').html(Math.round(80 * progress) + '<i>%</i>');
});

$('#circle4').circleProgress({
	value: 0.95,
	size: 150,
	fill: {
		gradient: ["#ed5745", "#124f80"]
	},
	startAngle: 4.7,
	thickness: 6,
}).on('circle-animation-progress', function (event, progress) {
	$(this).find('strong').html(Math.round(95 * progress) + '<i>%</i>');
});